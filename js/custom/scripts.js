(function ($) {
	$(document).ready(function () {
		$( ".datepicker-projects" ).datetimepicker({secondGrid: 10, hourGrid: 4, minuteGrid: 10, dateFormat: 'yy-mm-dd', timeFormat: 'hh:mm:ss'});

		$('[data-type="delete"]').click(function () {
			var that = $(this).parents('tr'),
				params = that.data();
			params.type = "delete";
			if(params.controller = 'users'){
				params.type = "delete-user";
			}
			console.log(params); // для дебага, дабы узнать что мы передаем все то, что нужно. Потом можно убрать.
			$.post('ajax_managers.php', params, function (data) {
				if (data == 'error') { // если не поняли, зачем здесь это условие, то поймете позже
					alert('Запис не видалено!');
				} else {
					that.html("");
				}
			});
		});
		$('[data-type="edit"]').click(function () {
			var that = $(this).parents('tr');
			that.addClass('show');
		});
		$('[data-type="cancel"]').click(function () {
			var that = $(this).parents('tr');
			that.removeClass('show');
		});
		$('[data-type="save"]').click(function () {
			var that = $(this).parents('tr'),
				params = that.data(),
				name = that.find('input[name="name"]').val(),
				description, deadline;
			params.type = "save";
			if(params.controller == "projects"){
				deadline = that.find('input[name="deadline"]').val();
				params.type = "save-project";
				if(deadline.trim() == ""){
					alert('Вкажіть кінцевий термін!');
					return;
				}
			}else{
				description = that.find('input[name="description"]').val();
			}
			if(name.trim() == ""){
				alert('Вкажіть назву!');
				return;
			}

			params.name = name;
			params.description = description;
			params.deadline = deadline;
			console.log(params); // для дебага, дабы узнать что мы передаем все то, что нужно. Потом можно убрать.
			$.post('ajax_managers.php', params, function (data) {
				if (data == 'error') { // если не поняли, зачем здесь это условие, то поймете позже
					alert('Запис не змінено!');
				} else {
					that.find('[data-name]').html(name);
					if(params.controller == "projects"){
						that.find('[data-deadline]').html(deadline);
					}else{
						that.find('[data-description]').html(description);
					}

					that.removeClass('show');
				}
			});
		});
		$('[data-type="save-user"]').click(function () {
			var that = $(this).parents('tr'),
				params = that.data();
			params.role_id = that.find('select[name="role_id"]').val();
			params.team_id = that.find('select[name="team_id"]').val();
			params.type = "save-user";

			console.log(params); // для дебага, дабы узнать что мы передаем все то, что нужно. Потом можно убрать.
			$.post('ajax_managers.php', params, function (data) {
				if (data == 'error') { // если не поняли, зачем здесь это условие, то поймете позже
					alert('Запис не змінено!');
				} else {
					alert('Запис змінено!');
				}
			});
		});
		$('[data-type="create"]').click(function () {
			var that = $(this).parents('tr'),
				params = that.data(),
				name = that.find('input[name="name"]').val(),
				description, deadline;
			params.type = "create";
			if(params.controller == "projects"){
				deadline = that.find('input[name="deadline"]').val();
				params.type = "create-project";
				if(deadline.trim() == ""){
					alert('Вкажіть кінцевий термін!');
					return;
				}
			}else{
				description = that.find('input[name="description"]').val();
			}
			if(name.trim() == ""){
				alert('Вкажіть назву!');
				return;
			}

			params.name = name;
			params.description = description;
			params.deadline = deadline;
			console.log(params); // для дебага, дабы узнать что мы передаем все то, что нужно. Потом можно убрать.
			$.post('ajax_managers.php', params, function (data) {
				if (data == 'error') { // если не поняли, зачем здесь это условие, то поймете позже
					alert('Запис не змінено!');
				} else {
					window.location.href = window.location.href;
				}
			});
		});
	});
})(jQuery);
