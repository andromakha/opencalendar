<?php

class Adminmodel extends CI_Model {

	function __construct() {
        parent::__construct();
    }
	function getAllUsers() {
		$query = $this->db->get('users');
        return $res = $query->result_array();
    }
    function getUser($id) {
        $this->db->where('user_id', $id);
        $query = $this->db->get('users');
        return $res = $query->result_array();
    }
    function getTeam($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('teams');
        return $res = $query->result_array();
    }
    function getRole($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('roles');
        return $res = $query->result_array();
    }
    function getListUsers() {
        $query = $this->db->query('SELECT users.*, teams.name as team, roles.name as role FROM teams, roles, users WHERE `users`.`team_id`=`teams`.`id` AND `users`.`role_id`=`roles`.`id`');
        return $res = $query->result_array();
    }
	function getAll($table) {
		$query = $this->db->get($table);
        return $res = $query->result_array();
    }
	function checkUser($username, $password) {
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		return $query = $this->db->get('users');
	}
	function checkNick($nick) {
		$this->db->where('username', $nick);
		return $query = $this->db->get('users');
	}
	function getTagsRu() {
		//$this->db->select('tags.id,tags_en,tags_ru');
		$this->db->order_by('tags_ru', 'asc');
		$query = $this->db->get('tags');
		return $query->result_array();
	}
	function getSelectedTagsRu($news_id) {
		$this->db->select('tags.id,tags.tags_en');
		$this->db->join('news_tags', 'news.id = news_tags.news_id', 'inner');
		$this->db->join('tags', 'news_tags.tags_id = tags.id', 'inner');
		$this->db->where('news.id', $news_id);
		$query = $this->db->get('news');
		return $query->result_array();
	}
}
?>