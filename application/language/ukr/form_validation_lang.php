<?php

$lang['required']			= "Поле %s обов'зкове для заповнення.";
$lang['isset']				= "The %s field must have a value.";
$lang['valid_email']		= "Введіть правильний e-mail";
$lang['valid_emails']		= "The %s field must contain all valid email addresses.";
$lang['valid_url']			= "The %s field must contain a valid URL.";
$lang['valid_ip']			= "The %s field must contain a valid IP.";
$lang['min_length']			= "Поле %s повинне містити не менше %s символів.";
$lang['max_length']			= "The %s field can not exceed %s characters in length.";
$lang['exact_length']		= "The %s field must be exactly %s characters in length.";
$lang['alpha']				= "The %s field may only contain alphabetical characters.";
$lang['alpha_numeric']		= "Поле %s може містити тільки алфавітно-цифрові символи.";
$lang['alpha_dash']			= "Поле %s може містити тільки алфавітно-цифрові символи, підкреслення та тире.";
$lang['numeric']			= "The %s field must contain only numbers.";
$lang['is_numeric']			= "The %s field must contain only numeric characters.";
$lang['integer']			= "The %s field must contain an integer.";
$lang['regex_match']		= "The %s field is not in the correct format.";
$lang['matches']			= "Поле %s повинне співпадати з полем %s.";
$lang['is_unique'] 			= "The %s field must contain a unique value.";
$lang['is_natural']			= "The %s field must contain only positive numbers.";
$lang['is_natural_no_zero']	= "The %s field must contain a number greater than zero.";
$lang['decimal']			= "The %s field must contain a decimal number.";
$lang['less_than']			= "The %s field must contain a number less than %s.";
$lang['greater_than']		= "The %s field must contain a number greater than %s.";


/* End of file form_validation_lang.php */
/* Location: ./application/language/ukr/form_validation_lang.php */