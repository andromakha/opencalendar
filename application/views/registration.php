<main>
    <section>
        <form action="register" id="registration" method="post">
            <h2>Реєстрація:</h2>
            <div class="line">
                <label>
                    <span>Прізвище:</span><span class="star">*</span>
                    <input type="text" name="last_name" value="<?php echo $registration['last_name']; ?>"/>
                    <span class="error"><?php echo form_error('last_name'); ?></span>
                </label>
            </div>
            <div class="line">
                <label>
                    <span>Ім'я:</span><span class="star">*</span>
                    <input type="text" name="first_name" value="<?php echo $registration['first_name']; ?>"/>
                    <span class="error"><?php echo form_error('first_name'); ?></span>
                </label>
            </div>
            <div class="line">
                <label>
                    <span>Стать:</span>
                    <select name="sex">
                        <option>Чоловік</option>
                        <option>Жінка</option>
                    </select>
                </label>
            </div>
            <div class="line">
                <label>
                    <span>Дата народження:</span>
                    <input type="text" class="datepicker-projects" name="birthday"/>
                </label>
            </div>
            <div class="line">
                <label>
                    <span>Контактний телефон:</span><span class="star">*</span>
                    <input type="tel" name="phone" value="<?php echo $registration['phone']; ?>"/>
                    <span class="error"><?php echo form_error('phone'); ?></span>
                </label>
            </div>
            <div class="line">
                <label>
                    <span>Електронна скринька:</span><span class="star">*</span>
                    <input type="email" name="email" value="<?php echo $registration['email']; ?>"/>
                    <span class="error"><?php echo form_error('email'); ?></span>
                </label>
            </div>
            <div class="line">
                <label>
                    <span>Skype:</span>
                    <input type="text" name="skype" value=""/>
                </label>
            </div>
            <div class="line">
                <label>
                    <span>Логін:</span><span class="star">*</span>
                    <input type="text" name="username" value="<?php echo $registration['username']; ?>"/>
                    <span class="error"><?php echo form_error('username'); ?></span>
                </label>
            </div>
            <div class="line">
                <label>
                    <span>Пароль:</span><span class="star">*</span>
                    <input type="password" name="password"/>
                    <span class="error"><?php echo form_error('password'); ?></span>
                </label>
            </div>
            <div class="line">
                <label>
                    <span>Повторити пароль:</span><span class="star">*</span>
                    <input type="password" name="passconf"/>
                    <span class="error"><?php echo form_error('passconf'); ?></span>
                </label>
            </div>
            <div class="line">
                <label>
                    <span>Місто:</span>
                    <input type="text" name="city" value=""/>
                </label>
            </div>
            <div class="line">
                <label>
                    <span>Навички:</span>
                    <textarea rows="2" cols="30" name="skills"><?php echo $registration['skills']; ?></textarea>
                </label>
            </div>
            <div class="line">
                <label>
                    <span>Інтереси:</span>
                    <textarea rows="2" cols="30" name="interests"><?php echo $registration['interests']; ?></textarea>
                </label>
            </div>
            <button type="submit">Відправити</button><br/>
            <div class="line">
                Усі поля позначені зірочкою - <span class="star">*</span>, обов'язкові для заповнення.
            </div>
        </form>
    </section>
</main>