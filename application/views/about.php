<main>
    <section id="about">
        <div id="block-about">
            <h3 class="questions">Виникли запитання? Знайшли помилку? </h3>
            <h3 class="questions">Чи маєте пропозиції щодо покращення сайту?</h3>
            <h3 class="questions">Тоді напишіть нам! Ми будемо Вам дуже вдячні!</h3>
            <p class="italic">Електронна адреса:</p>
            <p class="bold">andromakha@yandex.ua</p>
            <p class="italic">Skype:</p>
            <p class="bold">ellin.coleraine</p>
            <p class="italic">Телефон:</p>
            <p class="bold">+38 050 77 194 16</p>
            <p class="bold">+38 063 48 42 106</p>
        </div>
        <div id="form-contact">
            <h3 class="h3">Або зверніться за допомогою форми зворотнього зв'язку:</h3>
            <form action="contact-us" id="contact-us" method="post">
                <div class="line">
                    <label>
                        <span>Ваша електронна скринька:</span><span class="star">*</span>
                        <input type="email" name="email" value=""/>
                        <span class="error"><?php echo form_error('email'); ?></span>
                    </label>
                </div>
                <div class="line">
                    <label>
                        <span class="inscription">Ваше повідомлення:</span><span class="star">*</span>
                        <textarea id="message" rows="6" cols="24"></textarea>
                    </label>
                </div>
                <button type="submit">Відправити</button><br/>
                <div class="line">
                    Усі поля позначені зірочкою - <span class="star">*</span>, обов'язкові для заповнення.
                </div>
            </form>
        </div>
    </section>
</main>