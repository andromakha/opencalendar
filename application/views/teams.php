<main>
    <section id="control">
        <h2>Керування спільнотами:</h2>
        <table>
            <thead>
                <tr>
                    <th>№</th>
                    <th>Назва</th>
                    <th>Опис</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $count = count($teams);
                 for($i=0; $i<count($teams); $i++){?>
                    <tr data-id="<?php echo $teams[$i]['id']; ?>" data-controller="teams">
	                    <td><?php echo $i+1; ?></td>
	                    <td>
		                    <span data-name><?php echo $teams[$i]['name']; ?></span>
		                    <input type="text" name="name" value="<?php echo $teams[$i]['name']; ?>">
	                    </td>
	                    <td>
		                    <span data-description><?php echo $teams[$i]['description']; ?></span>
		                    <input type="text" name="description" value="<?php echo $teams[$i]['description']; ?>">
	                    </td>
	                    <td>
		                    <button data-type="edit">Редагувати</button>
		                    <button data-type="save">Зберегти</button>
		                    <button data-type="cancel">Скасувати</button>
	                    </td>
	                    <td><button data-type="delete">Видалити</button></td>
                    </tr>
                 <?php }?>
                <tr class="show" data-controller="teams">
	                <td><?php echo $count+1; ?></td>
	                <td><input type="text" name="name" ></td>
	                <td><input type="text" name="description" ></td>
	                <td><button data-type="create">Створити</button></td>
	                <td></td>
                </tr>
            </tbody>
        </table>
    </section>
</main>