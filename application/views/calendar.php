<main>
    <section>
        <div id="calendar"></div>
        <button id="add_event_button" class="add">+ ДОДАТИ</button>
        <div id="dialog-form" title="Подія">
            <p class="validateTips"></p>
            <form>
                <p>
	                <label for="event_title">Назва події: </label>
                    <input type="text" id="event_title" name="event_title" value="">
                </p>
                <p>
	                <label for="event_description">Опис події: </label>
                    <input type="text" id="event_description" name="event_description" value="">
                </p>
                <p>
	                <label for="event_start">Початкова дата: </label>
	                <input type="text" id="event_start" name="event_start" value="">
                </p>
                <p><label for="event_end">Кінцева дата: </label>
                    <input type="text" name="event_end" id="event_end"/>
                </p>
	            <p>
		            <label for="event_comment">Коментар: </label>
		            <textarea type="text" name="event_comment" id="event_comment"></textarea>
	            </p>
	            <p>
		            <label for="event_effort">Час виконання: </label>
		            <input type="text" name="event_effort" id="event_effort"/>
	            </p>
	            <p>
		            <label for="event_project_id">Проект: </label>
		            <textarea type="text" name="event_project_id" id="event_project_id"></textarea>
	            </p>
	            <p>
		            <label for="event_assign_id">Призначити: </label>
		            <textarea type="text" name="event_assign_id" id="event_assign_id"></textarea>
	            </p>
	            <input type="hidden" name="event_id" id="event_id" value="">
	            <input type="hidden" name="event_creator" id="event_creator" value="">
            </form>
        </div>
    </section>
</main>