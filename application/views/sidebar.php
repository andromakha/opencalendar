<aside>
    <nav id="top">
        <a  href="<?php echo base_url() . 'home' ?>"></a>
	    <a <? if ($page == 'home'):?> class="current_page" <? endif; ?> href="<?php echo base_url() . 'home' ?>">Головна</a>
	    <? if( !$this->session->userdata('user_logged')){?>
		    <a <? if ($page == 'about'):?> class="current_page" <? endif; ?> href="<?php echo base_url() . 'about' ?>">Контакти</a>
	    <?}else{
	        if($user[0] && $user[0]['role_id'] == '26'){?>
		        <h2>Керування</h2>
		        <a <? if ($page == 'roles'):?> class="current_page" <? endif; ?> href="<?php echo base_url() . 'roles' ?>">Ролі</a>
		        <a <? if ($page == 'projects'):?> class="current_page" <? endif; ?> href="<?php echo base_url() . 'projects' ?>">Проекти</a>
		        <a <? if ($page == 'teams'):?> class="current_page" <? endif; ?> href="<?php echo base_url() . 'teams' ?>">Спільноти</a>
		        <a <? if ($page == 'users'):?> class="current_page" <? endif; ?> href="<?php echo base_url() . 'users' ?>">Користувачі</a>
	        <?}else if($user[0] && $user[0]['role_id'] == '27'){?>
		        <a <? if ($page == 'calendar'):?> class="current_page" <? endif; ?> href="<?php echo base_url() . 'calendar' ?>">Календар</a>
		        <a <? if ($page == 'about'):?> class="current_page" <? endif; ?> href="<?php echo base_url() . 'about' ?>">Контакти</a>
		        <h2>Керування</h2>
		        <a <? if ($page == 'projects'):?> class="current_page" <? endif; ?> href="<?php echo base_url() . 'projects' ?>">Проекти</a>
		        <a <? if ($page == 'users'):?> class="current_page" <? endif; ?> href="<?php echo base_url() . 'users' ?>">Користувачі</a>
	        <?}else{?>
		        <a <? if ($page == 'calendar'):?> class="current_page" <? endif; ?> href="<?php echo base_url() . 'calendar' ?>">Календар</a>
		        <a <? if ($page == 'about'):?> class="current_page" <? endif; ?> href="<?php echo base_url() . 'about' ?>">Контакти</a>
	        <?}
	    } ?>
    </nav>
</aside>