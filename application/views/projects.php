<main>
    <section id="control">
        <h2>Керування проектами:</h2>
        <table>
            <thead>
                <tr>
                    <th>№</th>
                    <th>Назва</th>
                    <th>Кінцевий термін</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $count = count($projects);
                 for($i=0; $i<$count; $i++){?>
                    <tr data-id="<?php echo $projects[$i]['id']; ?>" data-controller="projects">
	                    <td><?php echo $i+1; ?></td>
	                    <td>
		                    <span data-name><?php echo $projects[$i]['name']; ?></span>
		                    <input type="text" name="name" value="<?php echo $projects[$i]['name']; ?>">
	                    </td>
	                    <td>
		                    <span data-deadline><?php echo $projects[$i]['deadline']; ?></span>
		                    <input type="text" name="deadline" class="datepicker-projects" value="<?php echo $projects[$i]['deadline']; ?>">
	                    </td>
	                    <td>
		                    <button data-type="edit">Редагувати</button>
		                    <button data-type="save">Зберегти</button>
		                    <button data-type="cancel">Скасувати</button>
	                    </td>
	                    <td><button data-type="delete">Видалити</button></td>
                    </tr>
                 <?php }?>
                <tr class="show" data-controller="projects">
	                <td><?php echo $count+1; ?></td>
	                <td><input type="text" name="name" ></td>
	                <td><input type="text" class="datepicker-projects" name="deadline" ></td>
	                <td><button data-type="create">Створити</button></td>
	                <td></td>
                </tr>
            </tbody>
        </table>
    </section>
</main>