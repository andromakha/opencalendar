<main>
    <section id="home">
        <h2 class="h2">Комплект інструментів для організації роботи</h2>
        <h3 class="h3">Як працювати з календарем?</h3>
        <p>Кожен наш користувач має змогу використовувати календар для своїх потреб,
            адже події записуються в особистому календарі.
            Для початку роботи необхідно створити новий обліковий запис,
            заповнивши відповідну форму реєстрації на сайті.&nbsp<a class="link" href="<?php echo base_url();?>login/register">реєстрація</a>
        </p>
        <h3 class="h3">Авторизуйтесь на сайті і розпочніть роботу!</h3>

        <p>Ви контролюєте час, а саме маєте можливість додавати та видаляти події,
            переглядати їх та змінювати. Завдяки зручному інтерфейсу це під силу навіть початківцям.<br/>
            Подію можна додавати у два способи:<br/>
               - натиснути відповідну кнопку &laquo;+ДОДАТИ&raquo;,<br/>
               - натиснути на певну дату на календарній сітці.
        </p>
        <div class="imag-content"><img src="../../images/content-1.png"/></div>
        <p>У відкритому вікні вкажіть параметри події, натисніть зберегти і готово, подію створено.
            Якщо припустились помилки, або ж змінились плани, то Ви завжди зможете змінити чи видалити подію.
            Для цього натисніть на відповідну подію і у відкритому вікні оберіть потрібне.
        </p>
        <p>Також застосовуйте календарне планування робіт у своїй компанії,
            адже цей веб-ресурс надасть змогу співробітникам стати більш організованими.
            Хочете, щоб процес планування робочого часу у вашій компанії став простим і дієвим?
            Позбавте себе &laquo;головного болю&raquo; чи зайвої паперової роботи.
            Користуйтесь онлайн-проектом для планування робіт.
        </p>
        <h3 class="h3">Об'єднуйтесь в спільноти та слідкуйте за подіями разом!</h3>

        <p>Обирайте серед користувачів групи керівника, підключайтесь до загального проекту.
            Менеджер керує завданнями в рамках проекту, враховує витрати часу і інших ресурсів,
            вносить зміни до загального плану, розподіляє і закріплює задачі за кожним виконавцем,
            контролює їх виконання.Поставити завдання співробітнику можна всього в «один клік»!
            Напишіть, що потрібно зробити, коли і хто за це відповідає.
            Далі - перевіряйте результат.
        </p>
        <div class="imag-content"><img src="../../images/content-3.png"/></div>
        <p>Усі задачі групи внесені у календар менеджера проектів.
            Календар кожного користувача відображає закріплені за ним завдання.
            Завдання інтегровані з календарями, так що ви завжди бачите,
            яке завдання до якого терміну необхібно виконати.
            Швидко переходьте до найважливіших завдань у роботі, які вимагають вашої уваги.
            Перегляньте скільки завдань всього, нових, прострочених?
            Контролюйте завдання, за які відповідаєте особисто, або спостерігайте чим завантажені підлеглі.
        </p>
    </section>
</main>