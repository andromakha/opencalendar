<main>
    <section id="control">

        <h2>Керування користувачами:</h2>
        <table>
            <thead>
                <tr>
                    <th>№</th>
                    <th>Прізвище</th>
                    <th>Ім’я</th>
	                <th>Профайл</th>
	                <th>Роль</th>
                    <th>Команда</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $count = count($users);
                 for($i=0; $i<count($users); $i++){?>
                    <tr data-id="<?php echo $users[$i]['user_id']; ?>" data-controller="users">
	                    <td><?php echo $i+1; ?></td>
	                    <td>
		                    <?php echo $users[$i]['firstName']; ?>
	                    </td>
	                    <td>
		                    <?php echo $users[$i]['lastName']; ?>
	                    </td>
	                    <td id="profile_a">
							<?php echo anchor('profile/'.$users[$i]['user_id'], 'Профайл'); ?>
	                    </td>

	                    <td>
		                    <?php  if ($user[0]['role_id'] == 27){?>
			                    <output value="<?php echo $users[$i]['role_id']; ?>"><?php echo $users[$i]['role']; ?></output>


		                    <?
		                    }else { ?>
                             <select name="role_id">
			                    <?php for ($j = 0; $j < count($roles); $j++) {
				                    $selected = "";
				                    $disabled = "";
				                    if ($users[$i]['role_id'] == $roles[$j]['id']) {
                                      ?>
                             <option selected="selected" value="<?php echo $roles[$j]['id']; ?>"><?php echo $roles[$j]['name']; ?></option>
			                    <?
				                    }else{?>

                                <option  value="<?php echo $roles[$j]['id']; ?>"><?php echo $roles[$j]['name']; ?></option>
                                  <?  }



			                    }
			                }
		                    ?>
		                    </select>
	                    </td>

	                    <td>
		                    <select name="team_id">
			                    <?php for($j=0; $j < count($teams); $j++){
				                    $selected = "";
				                    if ($users[$i]['team_id'] == $teams[$j]['id']){
					                    $selected = 'selected="selected"';
				                    }
				                    ?>
				                    <option <?php echo $selected?> value="<?php echo $teams[$j]['id']; ?>"><?php echo $teams[$j]['name']; ?></option>
			                    <?}?>
		                    </select>
	                    </td>
	                    <td>
		                    <?php if($users[$i]['role_id'] != '26' || $users[$i]['role'] != 'Адміністратор'){ ?>
			                    <button data-type="save-user">Зберегти</button>
		                    <?php } ?>
	                    </td>
	                    <td>
		                    <?php if($users[$i]['role_id'] != '26' || $users[$i]['role'] != 'Адміністратор'){ ?>
		                        <button data-type="delete">Видалити</button></td>
	                        <?php } ?>
                    </tr>
                 <?php }?>
            </tbody>
        </table>
    </section>
</main>