

<main>
    <section id="control">
        <h2>Особистий кабінет</h2>

        <div id="avatar">
            <span id="fullName"><?php echo $users[0]['firstName']. ' ' . $users[0]['lastName']?></span>
            <img src='../../images/user_avatar.png'/>
        </div>
        <div id="info">

            <table>
                <tbody>
                <tr class="h3">
                    <td>Контактна інформація:</td>
                </tr>
                <tr>
                    <td>Пошта:</td>
                    <td>
                        <span id="email"><?php echo $users[0]['email'] ?></span>
                    </td>
                </tr>
                <tr>
                    <td>Телефон:</td>
                    <td>
                        <span id="phone"><?php echo $users[0]['phone'] ?></span>
                    </td>
                </tr>
                <tr>
                    <td>Skype:</td>
                    <td>
                        <span id="skype"><?php echo $users[0]['skype'] ?></span>
                    </td>
                </tr>
                <tr class="h3">
                    <td>Загальні дані:</td>
                </tr>
                <tr>
                    <td>Дата народження:</td>
                    <td>
                        <span id="birthday"><?php echo $users[0]['birthday'] ?></span>
                    </td>
                </tr>
                <tr>
                    <td>Роль:</td>
                    <td>
                        <span id="role"><?php echo $role_user; ?></span>
                    </td>
                </tr>
                <tr>
                    <td>Спільнота:</td>
                    <td>
                        <span id="team"><?php echo $team_user; ?></span>
                    </td>
                </tr>
                <tr class="h3">
                    <td>Додаткова інформація:</td>
                </tr>
                <tr>
                    <td>Стать:</td>
                    <td>
                        <span id="sex"><?php echo $users[0]['sex'] ?></span>
                    </td>
                </tr>
                <tr>
                    <td>Місто:</td>
                    <td>
                        <span id="city"><?php echo $users[0]['city'] ?></span>
                    </td>
                </tr>
                <tr>
                    <td>Навички:</td>
                    <td>
                        <span id="skills"><?php echo $users[0]['skills'] ?></span>
                    </td>
                </tr>
                <tr>
                    <td>Інтереси:</td>
                    <td>
                        <span id="interests"><?php echo $users[0]['interests'] ?></span>
                    </td>
                </tr>
                <tr>
                    <td>Дата реєстрації:</td>
                    <td>
                        <span id="dataRegistration"><?php echo $users[0]['registration_date'] ?></span>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </section>
</main>