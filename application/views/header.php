<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>Open Calendar</title>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="<?php echo base_url();?>css/style_general.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/style_sidebar.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/style_header.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/style_main.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/style_footer.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/style_fc.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/fullcalendar.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/style_managers.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/style_profile.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/jquery-ui-1.8.11.custom.css" />
    <link rel = "shortcut icon" href="../../favicon.ico" type="image/x-icon"/>
    <script src="<?php echo base_url();?>js/jquery-1.6.3.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
    <script src="<?php echo base_url();?>js/jquery-ui-1.8.11.custom.min.js"></script>
    <script src="<?php echo base_url();?>js/fullcalendar.min.js"></script>
    <script src="<?php echo base_url();?>js/jquery-ui-timepicker-addon.js"></script>
    <script src="<?php echo base_url();?>js/custom/scripts.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            /* глобальные переменные */
	        var event_title = $('#event_title');
	        var event_description = $('#event_description');
	        var event_comment = $('#event_comment');
	        var event_effort = $('#event_effort');
	        var event_start = $('#event_start');
	        var event_end = $('#event_end');
	        var event_project_id = $('#event_project_id');
	        var event_creator = $('#event_creator');
	        var event_assign_id = $('#event_assign_id');

            var calendar = $('#calendar');
            var form = $('#dialog-form');
            var event_id = $('#event_id');
            var format = "MM/dd/yyyy HH:mm";
            /* кнопка добавления события */
            $('#add_event_button').button().click(function(){
                formOpen('add');
            });
            /** функция очистки формы */
            function emptyForm() {
                event_id.val("");
	            event_title.val("");
	            event_description.val("");
	            event_comment.val("");
	            event_effort.val("");
	            event_start.val("");
	            event_end.val("");
	            event_project_id.val("");
	            event_creator.val("");
	            event_assign_id.val("");
            }
            /* режимы открытия формы */
            function formOpen(mode) {
                if(mode == 'add') {
                    /* скрываем кнопки Удалить, Изменить и отображаем Добавить*/
                    $('#add').show();
                    $('#edit').hide();
                    $("#delete").button("option", "disabled", true);
                }
                else if(mode == 'edit') {
                    /* скрываем кнопку Добавить, отображаем Изменить и Удалить*/
                    $('#edit').show();
                    $('#add').hide();
                    $("#delete").button("option", "disabled", false);
                }
                form.dialog('open');
            }
            /* инициализируем Datetimepicker */
            event_start.datetimepicker({hourGrid: 4, minuteGrid: 10, dateFormat: 'mm/dd/yy'});
            event_end.datetimepicker({hourGrid: 4, minuteGrid: 10, dateFormat: 'mm/dd/yy'});
            /* инициализируем FullCalendar */
            calendar.fullCalendar({
                firstDay: 1,
                height: 500,
                editable: true,
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                monthNames: ['Січень','Лютий','Березень','Квітень','Травень','Червень','Липень','Серпень','Вересень','Жовтень','Листопад','Грудень'],
                monthNamesShort: ['Січ.','Лют.','Бер.','Квіт.','Трав.','Черв.','Лип.','Серп.','Вер.','Жовт.','Лист.','Груд.'],
                dayNames: ["Неділя","Понеділок","Вівторок","Середа","Четвер","П'ятниця","Субота"],
                dayNamesShort: ["НД","ПН","ВТ","СР","ЧТ","ПТ","СБ"],
                buttonText: {
                    prev: "&nbsp;&#9668;&nbsp;",
                    next: "&nbsp;&#9658;&nbsp;",
                    prevYear: "&nbsp;&lt;&lt;&nbsp;",
                    nextYear: "&nbsp;&gt;&gt;&nbsp;",
                    today: "Сьогодні",
                    month: "Місяць",
                    week: "Тиждень",
                    day: "День"
                },
	            weekends: false,
	            /* формат времени выводимый перед названием события*/
                timeFormat: 'H:mm',
                /* обработчик события клика по определенному дню */
                dayClick: function(date, allDay, jsEvent, view) {
                    var newDate = $.fullCalendar.formatDate(date, format);
                    event_start.val(newDate);
                    event_end.val(newDate);
                    formOpen('add');
                },
                /* обработчик кликак по событияю */
                eventClick: function(calEvent, jsEvent, view) {
	                event_id.val(calEvent.id);
	                event_title.val(calEvent.title);
	                event_description.val(calEvent.description);
	                event_comment.val(calEvent.comment);
	                event_effort.val(calEvent.effort);
	                event_start.val($.fullCalendar.formatDate(calEvent.start, format));
	                event_end.val($.fullCalendar.formatDate(calEvent.end, format));
	                event_project_id.val(calEvent.project_id);
	                event_creator.val(calEvent.creator);
	                event_assign_id.val(calEvent.assign_id);
                    formOpen('edit');
                },
                /* обработчик по переносу события мышкой */
                eventDrop: function(calEvent, jsEvent, view) {
	                event_id.val(calEvent.id);
	                event_title.val(calEvent.title);
	                event_description.val(calEvent.description);
	                event_comment.val(calEvent.comment);
	                event_effort.val(calEvent.effort);
	                event_start.val($.fullCalendar.formatDate(calEvent.start, format));
	                event_end.val($.fullCalendar.formatDate(calEvent.end, format));
	                event_project_id.val(calEvent.project_id);
	                event_creator.val(calEvent.creator);
	                event_assign_id.val(calEvent.assign_id);
                    $.ajax({
                        type: "POST",
                        url: "ajax.php",
                        data: {
	                        id: event_id.val(),
	                        title: event_title.val(),
	                        description: event_description.val(),
	                        comment: event_comment.val(),
	                        effort: event_effort.val(),
	                        start: event_start.val(),
	                        end: event_end.val(),
	                        project_id: event_project_id.val(),
	                        creator: event_creator.val(),
	                        assign_id: event_assign_id.val(),
	                        op: 'edit'
                        },
                        success: function(id){
                            calendar.fullCalendar('refetchEvents');
                        }
                    });
                },
                /* обработчик по растягиванию события мышкой */
                eventResize: function(calEvent, jsEvent, view) {
	                event_id.val(calEvent.id);
	                event_title.val(calEvent.title);
	                event_description.val(calEvent.description);
	                event_comment.val(calEvent.comment);
	                event_effort.val(calEvent.effort);
	                event_start.val($.fullCalendar.formatDate(calEvent.start, format));
	                event_end.val($.fullCalendar.formatDate(calEvent.end, format));
	                event_project_id.val(calEvent.project_id);
	                event_creator.val(calEvent.creator);
	                event_assign_id.val(calEvent.assign_id);
                    $.ajax({
                        type: "POST",
                        url: "ajax.php",
                        data: {
	                        id: event_id.val(),
	                        title: event_title.val(),
	                        description: event_description.val(),
	                        comment: event_comment.val(),
	                        effort: event_effort.val(),
	                        start: event_start.val(),
	                        end: event_end.val(),
	                        project_id: event_project_id.val(),
	                        creator: event_creator.val(),
	                        assign_id: event_assign_id.val(),
	                        op: 'edit'
                        },
                        success: function(id){
                            calendar.fullCalendar('refetchEvents');
                        }
                    });
                },
                /* источник записей */
                eventSources: [{
                    url: 'ajax.php',
                    type: 'POST',
                    data: {
                        op: 'source'
                    },
                    error: function() {
                        alert("Помилка з'єднання з джерелом даних!");
                    }
                }]
            });
            /* обработчик формы добавления */
            form.dialog({
                autoOpen: false,
                buttons: [{
                    id: 'add',
                    text: 'Додати',
                    click: function() {
                        $.ajax({
                            type: "POST",
                            url: "ajax.php",
                            data: {
	                            title: event_title.val(),
	                            description: event_description.val(),
	                            comment: event_comment.val(),
	                            effort: event_effort.val(),
	                            start: event_start.val(),
	                            end: event_end.val(),
	                            project_id: event_project_id.val(),
	                            creator: event_creator.val(),
	                            assign_id: event_assign_id.val(),
	                            op: 'add'
                            },
                            success: function(id){
                                calendar.fullCalendar('renderEvent', {
                                    id: id,
	                                title: event_title.val(),
	                                description: event_description.val(),
	                                comment: event_comment.val(),
	                                effort: event_effort.val(),
	                                start: event_start.val(),
	                                end: event_end.val(),
	                                project_id: event_project_id.val(),
	                                creator: event_creator.val(),
	                                assign_id: event_assign_id.val(),
	                                allDay: false
                                });
                                form.dialog('close');
                                emptyForm();

                            }
                        });
                    }
                },
                    {   id: 'edit',
                        text: 'Змінити',
                        click: function() {
                            $.ajax({
                                type: "POST",
                                url: "ajax.php",
                                data: {
	                                id: event_id.val(),
	                                title: event_title.val(),
	                                description: event_description.val(),
	                                comment: event_comment.val(),
	                                effort: event_effort.val(),
	                                start: event_start.val(),
	                                end: event_end.val(),
	                                project_id: event_project_id.val(),
	                                creator: event_creator.val(),
	                                assign_id: event_assign_id.val(),
	                                op: 'edit'
                                },
                                success: function(id){
                                    calendar.fullCalendar('refetchEvents');
                                }
                            });
                            $(this).dialog('close');
                            emptyForm();
                        }
                    },
                    {   id: 'cancel',
                        text: 'Скасувати',
                        click: function() {
                            $(this).dialog('close');
                            emptyForm();
                        }
                    },
                    {   id: 'delete',
                        text: 'Видалити',
                        click: function() {
                            $.ajax({
                                type: "POST",
                                url: "ajax.php",
                                data: {
                                    id: event_id.val(),
                                    op: 'delete'
                                },
                                success: function(id){
                                    calendar.fullCalendar('removeEvents', id);
                                }
                            });
                            $(this).dialog('close');
                            emptyForm();
                        },
                        disabled: true
                    }]
            });
        });
    </script>
</head>
<body>
    <div id="container">
        <header>
            <div id="reg_a">
                <a href="<?php echo base_url();?>login/register">Реєстрація</a>
            </div>
            <div id="wrapper_autorization">
                <?php
                    if($this->session->userdata('user_logged') == TRUE) {
                        echo "<img src='../../images/user_avatar.png'/>";
                        echo'<a href="/profile/'. $this->session->userdata('user_id') . '">';
                        echo ''.$this->session->userdata('firstName').' '.$this->session->userdata('lastName').'';
                        echo'</a>';
                        echo ' / '.anchor ('login/logout','<span id="test">Вихід</span>','title=""');
                    }
                    else { ?>
                        <form action="login" id="autorization" method="post">
                            <label>
                                <input type="text" name="user" placeholder="логін"/>
                                <span class="error"><?php echo form_error('user'); ?></span>
                            </label>
                            <label>
                                <input type="password" name="pass" placeholder="пароль"/>
                                <span class="error"><?php echo form_error('pass'); ?></span>
                            </label>
                            <label>
                                <button class="submit" type="submit">Вхід</button>
                            </label>
                        </form>
                <?php	}
                ?>
            </div>
        </header>
