<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Teams extends CI_Controller
{

	public function index()
	{
		$data = Array();

		$teams = $this->show();
		$data['page'] = 'teams';
		$data['teams'] = $teams;
		$this->load->view('header');
		$data['user'] = $this->adminmodel->getUser($this->session->userdata('user_id'));
		$this->load->view('sidebar', $data);
		$this->load->view('teams', $data);
		$this->load->view('footer');

	}

	private function show()
	{
		$data = $this->adminmodel->getAll('teams');
		return $data;
	}
}

?>