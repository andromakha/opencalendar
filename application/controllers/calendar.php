<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Calendar extends CI_Controller {

    public function index()
    {
		$data['page'] = 'calendar';
	    $this->load->view('header');
	    $data['user'] = $this->adminmodel->getUser($this->session->userdata('user_id'));
	    $this->load->view('sidebar', $data);
	    $this->load->view('calendar');
        $this->load->view('footer');
    }

}
?>