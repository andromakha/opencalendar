<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registration extends CI_Controller {

    public function index()
    {
	    $this->load->view('header');
	    $data['user'] = $this->adminmodel->getUser($this->session->userdata('user_id'));
	    $this->load->view('sidebar', $data);
        $this->load->view('registration');
        $this->load->view('footer');
    }
}
?>