<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller
{

	public function index()
	{
		$data = Array();

		$users = $this->show();
		$data['page'] = 'users';
		$data['users'] = $users;
		$data['roles'] = $this->adminmodel->getAll('roles');
		$data['teams'] = $this->adminmodel->getAll('teams');

		$this->load->view('header');
		$data['user'] = $this->adminmodel->getUser($this->session->userdata('user_id'));
		$this->load->view('sidebar', $data);
		$this->load->view('users', $data);
		$this->load->view('footer');

	}

	private function show()
	{
		$data = $this->adminmodel->getListUsers();
		return $data;
	}
}

?>