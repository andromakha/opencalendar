<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Roles extends CI_Controller
{

	public function index()
	{
		$data = Array();

		$roles = $this->show();
		$data['page'] = 'roles';
		$data['roles'] = $roles;
		$this->load->view('header');
		$data['user'] = $this->adminmodel->getUser($this->session->userdata('user_id'));
		$this->load->view('sidebar', $data);
		$this->load->view('roles', $data);
		$this->load->view('footer');

	}

	private function show()
	{
		$data = $this->adminmodel->getAll('roles');
		return $data;
	}
}

?>