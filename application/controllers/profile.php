<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

    public function index()
    {
        $data = Array();

        $users = $this->show();
        $team_user = $this->showteam($users[0]['team_id']);
        $role_user = $this->showrole($users[0]['role_id']);
        $data['team_user'] = $team_user[0]['name'];
        $data['role_user'] = $role_user[0]['name'];
        $data['page'] = 'users';
        $data['users'] = $users;
	    $this->load->view('header');
	    $data['user'] = $this->adminmodel->getUser($this->session->userdata('user_id'));
	    $this->load->view('sidebar', $data);
	    $this->load->view('profile', $data);
        $this->load->view('footer');
    }
    private function show()
    {
        $data = $this->adminmodel->getUser($this->session->userdata('user_id'));
        return $data;
    }
    private function showteam($result)
    {
        $data = $this->adminmodel->getTeam($result);
        return  $data;
    }
    private function showrole($result)
    {
        $data = $this->adminmodel->getRole($result);
        return  $data;
    }
	
	function user($user_id) {
		if( !$this->session->userdata('user_logged')) {
			redirect('');
		} else {
		$data['page'] = 'users';
		$data['users'] = $this->adminmodel->getUser($user_id);
		$users = $this->adminmodel->getUser($user_id);
		$team_user = $this->showteam($users[0]['team_id']);
        $role_user = $this->showrole($users[0]['role_id']);
        $data['team_user'] = $team_user[0]['name'];
        $data['role_user'] = $role_user[0]['name'];
		$data['user'] = $this->adminmodel->getUser($this->session->userdata('user_id'));
		
		$this->load->view('header');
		$this->load->view('sidebar', $data);
	    $this->load->view('profile', $data);
        $this->load->view('footer');
		}
    }
}
?>