<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Projects extends CI_Controller
{

	public function index()
	{
		$data = Array();

		$projects = $this->show();
		$data['page'] = 'projects';
		$data['projects'] = $projects;
		$this->load->view('header');
		$data['user'] = $this->adminmodel->getUser($this->session->userdata('user_id'));
		$this->load->view('sidebar', $data);
		$this->load->view('projects', $data);
		$this->load->view('footer');

	}

	private function show()
	{	
		$data = $this->adminmodel->getAll('projects');
		return $data;
	}
}

?>