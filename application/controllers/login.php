<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->lang->load('form_validation', 'ukr');
	}

	function index()
	{
		$this->form_validation->set_rules('user', "Ім'я", 'trim|required|min_length[3]|xss_clean|callback_isEmptyNick');
		$this->form_validation->set_rules('pass', 'Пароль', 'trim|required|min_length[3]|md5');
		if ($this->form_validation->run() == FALSE) {
			$data['page'] = 'home';
			$data['title'] = "Авторизація";
			$this->load->view('header', $data);
			$data['user'] = $this->adminmodel->getUser($this->session->userdata('user_id'));
			$this->load->view('sidebar', $data);
			$this->load->view('home');
			$this->load->view('footer');
		} else {
			//если пользователь нажал добавить и все поля заполнены то получаем значение полей из $_POST массива
			$username = $this->input->post('user', TRUE);
			$password = $this->input->post('pass', TRUE);
			$user = $this->adminmodel->checkUser($username, $password);
			if ($user->num_rows == 1) {   //если такой пользователь имеется
				$user = $user->row();
				$session_data = array(
					'user_logged' => TRUE,
					'user_id' => $user->user_id,
					'username' => $user->username,
					'password' => $user->password,
					'firstName' => $user->firstName,
					'lastName' => $user->lastName
				);
				$this->session->set_userdata($session_data);
				/*$data['page'] = 'calendar';
				$data['title'] = 'SUCCESS!';
				$this->load->view('sidebar', $data);
				$this->load->view('header', $data);
				$this->load->view('calendar');
				$this->load->view('footer');*/
				header("Location:".$_SERVER['HTTP_REFERER']);
			} else {
				$data['page'] = 'about';
				$this->load->view('header', $data);
				$data['user'] = $this->adminmodel->getUser($this->session->userdata('user_id'));
				$this->load->view('sidebar', $data);
				$this->load->view('about');
				$this->load->view('footer');
			}
		}
	}

	function logout()
	{
		$this->session->sess_destroy();
		header("Location: /");
	}

	function isEmptyNick($nick)
	{
		$nick_exist = $this->adminmodel->checkNick($nick);
		if ($nick_exist->num_rows < 1) {   //если такой пользователь имеется
			$this->form_validation->set_message('isEmptyNick', "Такий користувач не існує.");
			return false;
		} else {
			return true;
		}
	}

	function check_nick($nick)
	{
		$nick_exist = $this->adminmodel->checkNick($nick);
		if ($nick_exist->num_rows >= 1) {   //если такой пользователь имеется
			$this->form_validation->set_message('check_nick', "Такий користувач існує, виберіть інше ім'я.");
			return false;
		} else {
			return true;
		}
	}

	function register()
	{
		$this->form_validation->set_rules('last_name', 'Прізвище', 'trim|required');
		$this->form_validation->set_rules('first_name', 'Ім’я', 'trim|required');
		$this->form_validation->set_rules('phone', 'Телефон', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('username', "Ім'я", 'trim|required|min_length[3]|max_length[32]|xss_clean|alpha_dash|callback_check_nick');
		$this->form_validation->set_rules('password', 'Пароль', 'trim|required|min_length[3]|matches[passconf]|md5');
		$this->form_validation->set_rules('passconf', 'Повторити пароль', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$registryData = $this->prepareData();
			$data['registration'] = $registryData;
			$data['page'] = '';
			$data['title'] = 'Реєстрація';
			$this->load->view('header', $data);
			$data['user'] = $this->adminmodel->getUser($this->session->userdata('user_id'));
			$this->load->view('sidebar', $data);
			$this->load->view('registration', $data);
			$this->load->view('footer');
		} else {
			//если пользователь нажал добавить и все поля заполнены то  получаем значение полей из $_POST массива

			$username = $this->input->post('username', TRUE);
			$password = $this->input->post('password', TRUE);
			$lastName = $this->input->post('last_name', TRUE);
			$firstName = $this->input->post('first_name', TRUE);
			$email = $this->input->post('email', TRUE);
			$phone = $this->input->post('phone', TRUE);
			$skills = $this->input->post('skills', TRUE);
			$skype = $this->input->post('skype', TRUE);
			$city = $this->input->post('city', TRUE);
			$interests = $this->input->post('interests', TRUE);
			$sex = $this->input->post('sex', TRUE);
			$birthday = $this->input->post('birthday', TRUE);
			$ip = $this->input->ip_address();
			$data = array(
				//все значения заносим в массив
				'role_id' => 28,
				'team_id' => 1,
				'username' => $username,
				'password' => $password,
				'email' => $email,
				'registration_date' => date('Y-m-d H:i:s'),
				'lastName' => $lastName,
				'firstName' => $firstName,
				'phone' => $phone,
				'skills' => $skills,
				'skype' => $skype,
				'sex' => $sex,
				'city' => $city,
				'birthday' => $birthday,
				'interests' => $interests,
				'ip' => $ip,
				'block' => '0'
			);
			$this->db->insert('users', $data); // в таблицу users добавляем нового юзверя

			$data['page'] = '';
			$data['title'] = 'Реєстрація успішно завершена!';
			?>
			<script>
				alert('Реєстрація успішно завершена!');
			</script>
			<?
			$this->load->view('header', $data);
			$data['user'] = $this->adminmodel->getUser($this->session->userdata('user_id'));
			$this->load->view('sidebar', $data);
			$this->load->view('home');
			$this->load->view('footer');

		}
	}

	function prepareData(){
		$data = Array();
		$data['last_name'] = $this->input->post('last_name', TRUE);
		$data['first_name'] = $this->input->post('first_name', TRUE);
		$data['phone'] = $this->input->post('phone', TRUE);
		$data['email'] = $this->input->post('email', TRUE);
		$data['username'] = $this->input->post('username', TRUE);
		$data['skills'] = $this->input->post('skills', TRUE);
		$data['interests'] = $this->input->post('interests', TRUE);
		$data['password'] = "";
		$data['passconf'] = "";
		return $data;
	}

	function users()
	{
		$data['page'] = 'admin';
		$data['page_admin'] = 'users';
		$data['title'] = "users";
		$data['users'] = $this->adminmodel->getAllUsers();
		if ($data['users'] == false) {
			$data['errDescription'] = "Error!";
		}
		$this->load->view('admin/header', $data);
		$this->load->view('admin/all-users', $data);
		$this->load->view('dashboard');
		$this->load->view('footer');
	}

	function edit_user($id)
	{
		echo "edit_user" . $id;
	}

	function delete_user($id)
	{
		echo "delete_user" . $id;
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */