-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 24, 2015 at 12:45 AM
-- Server version: 5.5.25
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `opencalendar`
--

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `deadline` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `name`, `deadline`) VALUES
(2, 'Захист диплому', '2015-06-27 09:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`) VALUES
(26, 'Адміністратор', 'Повний доступ'),
(27, 'Менеджер проектів', 'Доступ до даних'),
(28, 'Користувач', 'По замовчуванню');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `description` text,
  `comment` text,
  `effort` int(4) NOT NULL DEFAULT '0',
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `creator` int(10) NOT NULL,
  `assign_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`,`assign_id`),
  KEY `assign_id` (`assign_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `title`, `description`, `comment`, `effort`, `start`, `end`, `project_id`, `creator`, `assign_id`) VALUES
(21, 'testsdf', 'testsdf', 'wer34234', 4, '2015-06-24 15:00:00', '2015-06-28 13:00:00', 2, 0, 65);

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE IF NOT EXISTS `teams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `description` text CHARACTER SET ucs2 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `id_2` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `name`, `description`) VALUES
(1, 'Команда №1', 'Моя команда'),
(2, 'DreamTeam', 'Project №1');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `team_id` int(10) unsigned NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `firstName` varchar(255) NOT NULL DEFAULT '',
  `lastName` varchar(255) NOT NULL DEFAULT '',
  `phone` varchar(255) NOT NULL DEFAULT '',
  `skills` text NOT NULL,
  `interests` text NOT NULL,
  `registration_date` datetime NOT NULL,
  `ip` varchar(15) DEFAULT NULL,
  `block` tinyint(1) DEFAULT NULL,
  `skype` varchar(50) NOT NULL,
  `sex` varchar(10) NOT NULL,
  `city` varchar(20) NOT NULL,
  `birthday` datetime NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `user_id` (`user_id`),
  KEY `team_id` (`team_id`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=72 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `role_id`, `team_id`, `username`, `password`, `email`, `firstName`, `lastName`, `phone`, `skills`, `interests`, `registration_date`, `ip`, `block`, `skype`, `sex`, `city`, `birthday`) VALUES
(61, 27, 1, 'andromakha', '5a105e8b9d40e1329780d62ea2265d8a', 'andromakha@yandex.ua', 'Катерина', 'Опрелянська', '050 77 194 16', 'Навички', 'Інтереси', '2015-06-18 16:19:30', '127.0.0.1', 0, '', '', '', '0000-00-00 00:00:00'),
(62, 26, 1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'andromakha@yandex.ua', 'Admin', 'Admin', '1234567890', '', '', '2015-06-22 21:14:23', '127.0.0.1', 0, '', '', '', '0000-00-00 00:00:00'),
(64, 27, 2, 'project_manager', 'd4277f68d6be7806b30501edfcfd0fd0', 'test@ty.com', 'John', 'Lane', '0961234567', '', '', '2015-06-22 21:52:45', '127.0.0.1', 0, '', '', '', '0000-00-00 00:00:00'),
(65, 28, 2, 'developer_1', 'cea099a8f5ac3e289e317d461beb9261', 'test@ty.com', 'Freddy', 'Bob', '0639876543', '', '', '2015-06-22 21:54:03', '127.0.0.1', 0, '', '', '', '0000-00-00 00:00:00'),
(66, 28, 2, 'developer_2', 'f77648e5d3e027222417fcba0f7291cb', 'test@ty.com', 'Sam', 'Switch', '09312349876', '', '', '2015-06-22 21:55:02', '127.0.0.1', 0, '', '', '', '0000-00-00 00:00:00'),
(67, 28, 2, 'designer', '0ecee728bf87a4c1a02883004044dcd5', 'test@ty.com', 'Lim', 'Chan', '0954567893', '', '', '2015-06-22 21:58:50', '127.0.0.1', 0, '', '', '', '0000-00-00 00:00:00'),
(68, 27, 2, 'tester', '098f6bcd4621d373cade4e832627b4f6', 'test@ty.com', 'Rick', 'Johnson', '0678763452', '', '', '2015-06-22 22:02:11', '127.0.0.1', 0, '', '', '', '0000-00-00 00:00:00'),
(71, 27, 1, 'test', '098f6bcd4621d373cade4e832627b4f6', 'test@test.test', 'test', 'test', '+380000000000', '', '', '2015-06-23 21:53:04', '127.0.0.1', 0, '', 'Чоловік', 'Житомир', '2015-06-18 00:00:00');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tasks`
--
ALTER TABLE `tasks`
  ADD CONSTRAINT `tasks_ibfk_2` FOREIGN KEY (`assign_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tasks_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`),
  ADD CONSTRAINT `users_ibfk_3` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
